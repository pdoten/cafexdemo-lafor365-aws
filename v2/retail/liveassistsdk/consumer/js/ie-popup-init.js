
// shims 
window.initShims = function() {
    window.html2canvas = AssistIFrame.html2canvas;
    window.PDFJS = AssistIFrame.PDFJS;
    window.MutationObserver = AssistIFrame.MutationObserverAPI;
}

window.init = function(configuration) {
    
    var config = {};
    for (var item in configuration) {
        config[item] = configuration[item];
    }
    
    console.log("config:");
    console.log(config);
    window.AssistConfig = new window.Config(config);
    console.log(AssistConfig);
    
    if (AssistConfig.hasDestination() == true) {
        AssistUtils.loadScript("js/assist-csdk.js", document, function() {
            AssistCSDK.loadScripts(start, true);
        });
    } else {
        start();
    }
    
    function start() {
        i18n.init({debug:true, ns:{namespaces:['assistI18n']},useCookie: false, lng:lang, fallbackLng: 'en', resGetPath: '../shared/locales/__ns__.__lng__.json'}, function(t) {
            $("html").i18n();
            
            window.initShims();
            
            if (AssistConfig.hasDestination() == true) {
                function startOnUC() {
                    if (typeof UC == 'undefined') {
                        console.log("No UC yet, waiting..");
                        setTimeout(function() {
                            startOnUC();
                        }, 100);
                    } else {
                        AssistSDK.startSupport();
                    }
                }
                startOnUC();
            } else {
                AssistSDK.startSupport();
            }
        });
    }
}

window.AssistLoaded = true;
if ("onAssistLoaded" in window) {
    window.onAssistLoaded(window);
}