;(function(targetWindow, window) {
    
    var id = 0;
    var MO = targetWindow.MutationObserver;
    window.MutationObserverAPI = function MutationObserver(callback) {
        
        var mutationObserver = new MO(function(mutations) {
                callback(mutations);
        });
        
        this.observe = function(element, config) {
            var newConfig = {};
            
            for (var item in config) {
                newConfig[item] = config[item];
            }

            mutationObserver.observe(element, newConfig);
        }
        
        this.takeRecords = function() {
            return mutationObserver.takeRecords();
        };
        
        this.disconnect = function() {
            mutationObserver.disconnect();
        };
    };
    
})(window.opener || window.parent, window);